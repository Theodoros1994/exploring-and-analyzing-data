install.packages("devtools")
install.packages("ggplot2")
install.packages("moments")
install.packages("MASS")
install.packages("Hmisc")
install.packages("coin")
install.packages("car")
install.packages("pgirmess")
install.packages("multcomp")
install.packages("reshape")
install.packages("WRS2")
install.packages("effsize")
install.packages("comp.es")
install.packages("Hmisc")
install.packages("pastecs")
install.packages("Hmisc")
install.packages("Hmisc")
library(coin) # # for various mean comparison tests using permutation
library(car) # Levene's and Bartlett's test


library(pgirmess)

library(sjstats)
library(multcomp)
library(reshape)

library(WRS2)


library(effsize)


library(compute.es)
library(car)

library(multcomp)
library(pastecs)
library(WRS2)